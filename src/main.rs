use std::fs::File;
use std::io;
use std::io::BufRead;
use std::path::Path;

fn main() {
    build_measurement_triplets();
}

fn build_measurement_triplets() {
    let mut matrix: Vec<[i32;3]> = Vec::new();
    if let Ok(lines) = read_lines("./input.txt") {
        let mut count =  0;
        for line in lines {
            if let Ok(depth) = line {
                matrix.push([0i32; 3]);
                let value = depth.parse::<i32>().unwrap();
                matrix[count][0] = value;
                if count >= 1 {
                    matrix [count-1][1] = value;
                }
                if count >= 2 {
                    matrix [count-2][2] = value;
                }
                count += 1;
            }
        }
    }
    let mut previous_triplet_sum: Option<i32>  = None;
    let mut increase_count = 0;
    for line in matrix {
        if line[2] != 0 {
            let current_triplet_sum = line[0] + line[1] + line[2];
            if let Some(prev) = previous_triplet_sum {
                if current_triplet_sum > prev {
                    increase_count += 1;
                    println!("{} {} {} increase", line[0], line[1], line[2]);
                } else {
                    println!("{} {} {} decrease", line[0], line[1], line[2]);
                }
            } else {
                println!("{} {} {} N/A", line[0], line[1], line[2]);
            }
            previous_triplet_sum = Some(current_triplet_sum);
        }
    }
    println!("increase count = {}", increase_count)
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
